<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class modelCatatanLogin extends CI_Model {
	public function getData($where='')
	{
		$data = $this->db->query("SELECT tb_log.*, tb_siswa.id_siswa, tb_siswa.nama, tb_sekolah.id_sekolah, tb_sekolah.nama_sekolah FROM tb_log INNER JOIN tb_siswa ON tb_siswa.id_siswa=tb_log.id_siswa INNER JOIN tb_sekolah ON tb_sekolah.id_sekolah=tb_log.id_sekolah ".$where."");
		return $data->result_array();
	}
}