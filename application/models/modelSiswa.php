<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class modelSiswa extends CI_Model {
	public function getData($where='')
	{
		$data = $this->db->query("SELECT * FROM tb_siswa INNER JOIN tb_sekolah ON tb_sekolah.id_sekolah=tb_siswa.id_sekolah ".$where." ORDER BY tb_siswa.nama");
		return $data->result_array();
	}
	public function belumAbsen($time)
	{
		$data = $this->db->query("SELECT tb_log.*, tb_siswa.nama FROM tb_log INNER JOIN tb_siswa ON tb_siswa.id_siswa=tb_log.id_siswa WHERE tb_log.login_tgl = '".$time."'");
		return $data->result_array();
	}
}