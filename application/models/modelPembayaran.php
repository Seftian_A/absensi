<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class modelPembayaran extends CI_Model {
	public function getData($where='')
	{
		$data = $this->db->query("SELECT tb_detail_pembayaran.id_detail_pembayaran, tb_detail_pembayaran.bayar, tb_pembayaran.tot_bayar, tb_detail_pembayaran.tanggal_bayar,tb_detail_pembayaran.id_siswa, tb_detail_pembayaran.status AS status_detail, tb_pembayaran.status AS status_bayar, tb_siswa.*, tb_sekolah.* FROM tb_detail_pembayaran INNER JOIN tb_pembayaran ON tb_detail_pembayaran.id_detail_pembayaran=tb_pembayaran.id_detail_pembayaran RIGHT JOIN tb_siswa ON tb_pembayaran.id_siswa=tb_siswa.id_siswa INNER JOIN tb_sekolah ON tb_siswa.id_sekolah=tb_sekolah.id_sekolah GROUP BY tb_siswa.id_siswa ".$where."");
		return $data->result_array();
	}
	public function cekPembayaran($where)
	{
		$data = $this->db->query("SELECT * FROM tb_pembayaran WHERE id_siswa = '".$where."'");
		return $data;
	}
	public function cekDetailPembayaran($where)
	{
		$data = $this->db->query("SELECT * FROM tb_detail_pembayaran WHERE id_siswa = '".$where."' GROUP BY id_siswa");
		return $data;
	}
	
}