<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class modelCore extends CI_Model {
	public function getData($tabel, $where='')
	{
		$data = $this->db->query("SELECT * FROM ".$tabel." ".$where."");
		return $data->result_array();
	}
	public function insertData($tabel, $data)
	{
		$data = $this->db->insert($tabel, $data);
		return $data;
	}
	public function updateData($tabel, $data, $where)
	{
		$data = $this->db->update($tabel, $data, $where);
		return $data;
	}
	public function deleteData($tabel, $where)
	{
		$data = $this->db->delete($tabel, $where);
		return $data;
	}
	public function countUser()
	{
		$data = $this->db->query("SELECT COUNT(id_user) AS tot_user FROM tb_user");
		return $data->result_array();
	}
	public function countSiswa()
	{
		$data = $this->db->query("SELECT COUNT(id_siswa) AS tot_siswa FROM tb_siswa");
		return $data->result_array();
	}
	public function countSekolah()
	{
		$data = $this->db->query("SELECT COUNT(id_sekolah) AS tot_sekolah FROM tb_sekolah");
		return $data->result_array();
	}
	public function countLog()
	{
		$data = $this->db->query("SELECT COUNT(id_log) AS tot_log FROM tb_log WHERE login_tgl = '".date('Y-m-d')."'");
		return $data->result_array();
	}
}