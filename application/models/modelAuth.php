<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class modelAuth extends CI_Model {
	public function getData($data1, $data2)
	{
		$data = $this->db->query("SELECT * FROM tb_siswa INNER JOIN tb_sekolah ON tb_sekolah.id_sekolah=tb_siswa.id_sekolah WHERE tb_siswa.nama = '".$data1."' AND tb_siswa.password='".$data2."'");
		return $data;
	}
	public function getLog($id, $date)
	{
		$data = $this->db->query("SELECT * FROM tb_log WHERE id_siswa = '".$id."' AND login_tgl = '".$date."'");
		return $data;
	}
	public function cekSiswa($ip)
	{
		$data = $this->db->query("SELECT * FROM tb_siswa WHERE ip_address = '".$ip."'");
		return $data;
	}
	public function insertData($tabel, $data, $password)
	{
		$md5 = md5($password); 
		$data = $this->db->set('password2', $password)
						 ->set('password', $md5)
						 ->set('status', 1)
						 ->insert($tabel, $data);
		return $data;
	}
	public function updateLog($tabel, $data, $id, $time)
	{
		$data = $this->db->where('id_siswa', $id)
						 ->where('login_tgl', $time)
						 ->update($tabel, $data);
		return $data;
	}
}