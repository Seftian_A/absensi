<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('modelCore');
	}
    public function index()
    {
    	$dataUser = $this->modelCore->countUser();
    	$dataSiswa = $this->modelCore->countSiswa();
    	$dataSekolah = $this->modelCore->countSekolah();
    	$dataLog = $this->modelCore->countLog();
        $this->load->view('content/dashboard', array(
        	'dataUser'=>$dataUser,
        	'dataSiswa'=>$dataSiswa,
        	'dataSekolah'=>$dataSekolah,
        	'dataLog'=>$dataLog
        ));
    }
}

