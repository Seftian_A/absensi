<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LaporanPembayaran extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('modelCore');
		$this->load->model('modelPembayaran');
	}
    public function index()
    {
    	$data = $this->modelPembayaran->getData();
        $this->load->view('content/laporanPembayaran/index', array(
        	'data'=>$data
        ));
    }
}

