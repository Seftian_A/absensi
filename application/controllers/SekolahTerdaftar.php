<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SekolahTerdaftar extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('modelCore');
	}
    public function index()
    {
    	$data = $this->modelCore->getData('tb_sekolah');
        $this->load->view('content/sekolahTerdaftar/index', array(
        	'data'=>$data
        ));
	}
	public function simpan()
	{
		$nama = $_POST['nama_sekolah'];

		$data = array(
			'nama_sekolah'=>$nama
		);

		$simpan = $this->modelCore->insertData('tb_sekolah', $data);
		if($simpan>=1){
			redirect('SekolahTerdaftar');
		}else{
			echo"Gagal";
		}
	}
	public function delete($id)
	{
		$where = array(
			'id_sekolah'=>$id
		);

		$delete = $this->modelCore->deleteData('tb_sekolah', 'id_sekolah = '.$where);
		if($delete >= 1){
			redirect('SekolahTerdaftar');
		}else{
			echo "Gagal";
		}
	}
}

