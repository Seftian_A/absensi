<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('modelCore');
		$this->load->model('modelPembayaran');
	}
    public function index()
    {
    	$data = $this->modelPembayaran->getData();

        $this->load->view('content/pembayaran/index', array(
        	'data'=>$data
        ));
    }
    public function cicil()
    {
        $id_siswa = $_POST['id_siswa'];
        $bayar = $_POST['jumlah_bayar'];
        $date = date('Y-m-d');
        $status = 1;
        $id_detail_pembayaran = random_string('numeric', 4);

        $data_detail = array(
            'id_detail_pembayaran'=>$id_detail_pembayaran,
            'id_siswa'=>$id_siswa,
            'bayar'=>$bayar,
            'tanggal_bayar'=>$date,
            'status'=>$status
        );

        $simpan = $this->modelCore->insertData('tb_detail_pembayaran', $data_detail);
        if($simpan>=1){
            $cek = $this->modelPembayaran->cekPembayaran($id_siswa)->num_rows();
            $dataPembayaran = $this->modelPembayaran->cekDetailPembayaran($id_siswa)->result_array();
            foreach($dataPembayaran as $pembayaran){
                $data = array(
                    'id_detail_pembayaran'=>$pembayaran['id_detail_pembayaran'],
                    'tot_bayar'=>$pembayaran['bayar']+$bayar,
                    'id_siswa'=>$id_siswa,
                    'status'=>0
                );
                if($cek>0){
                    $update = $this->modelCore->updateData('tb_pembayaran', $data, 'id_siswa ='.$id_siswa);
                }else{
                    $simpan2 = $this->modelCore->insertData('tb_pembayaran', $data);
                }
            }
        }
    }
    public function bulanIni()
    {
        $id_siswa = $_POST['id_siswa'];
        $bayar = $_POST['jumlah_bayar'];
        $date = date('Y-m-d');
        $status = 2;
        $id_detail_pembayaran = random_string('numeric', 4);

        $data_detail = array(
            'id_detail_pembayaran'=>$id_detail_pembayaran,
            'id_siswa'=>$id_siswa,
            'bayar'=>$bayar,
            'tanggal_bayar'=>$date,
            'status'=>$status
        );

        $simpan = $this->modelCore->insertData('tb_detail_pembayaran', $data_detail);
        if($simpan>=1){
            $cek = $this->modelPembayaran->cekPembayaran($id_siswa)->num_rows();
            $dataPembayaran = $this->modelPembayaran->cekDetailPembayaran($id_siswa)->result_array();
            foreach($dataPembayaran as $pembayaran){
                $data = array(
                    'id_detail_pembayaran'=>$pembayaran['id_detail_pembayaran'],
                    'tot_bayar'=>$pembayaran['bayar']+$bayar,
                    'id_siswa'=>$id_siswa,
                    'status'=>1
                );
                if($cek>0){
                    $update = $this->modelCore->updateData('tb_pembayaran', $data, 'id_siswa ='.$id_siswa);
                }else{
                    $simpan2 = $this->modelCore->insertData('tb_pembayaran', $data);
                }
            }
        }
    }
    public function semuaBulan()
    {
        $id_siswa = $_POST['id_siswa'];
        $bayar = $_POST['jumlah_bayar'];
        $date = date('Y-m-d');
        $status = 3;
        $id_detail_pembayaran = random_string('numeric', 4);

        $data_detail = array(
            'id_detail_pembayaran'=>$id_detail_pembayaran,
            'id_siswa'=>$id_siswa,
            'bayar'=>$bayar,
            'tanggal_bayar'=>$date,
            'status'=>$status
        );

        $simpan = $this->modelCore->insertData('tb_detail_pembayaran', $data_detail);
        if($simpan>=1){
            $cek = $this->modelPembayaran->cekPembayaran($id_siswa)->num_rows();
            $dataPembayaran = $this->modelPembayaran->cekDetailPembayaran($id_siswa)->result_array();
            foreach($dataPembayaran as $pembayaran){
                $data = array(
                    'id_detail_pembayaran'=>$pembayaran['id_detail_pembayaran'],
                    'tot_bayar'=>$pembayaran['bayar']+$bayar,
                    'id_siswa'=>$id_siswa,
                    'status'=>2
                );
                if($cek>0){
                    $update = $this->modelCore->updateData('tb_pembayaran', $data, 'id_siswa ='.$id_siswa);
                }else{
                    $simpan2 = $this->modelCore->insertData('tb_pembayaran', $data);
                }
            }
        }
    }
}

