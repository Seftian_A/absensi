<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('modelCore');
		$this->load->model('modelAuth');
	}
	public function index()
	{
		$this->load->view('layouts/login');
	}
	public function login()
	{
		$nama = $_POST['username'];
		$password = md5($_POST['password']);
		$time = date('h:i:s');
		$date = date('Y-m-d');
		$ip = $this->input->ip_address();

		$cek = $this->modelAuth->getData($nama, $password)->num_rows();
		if($cek>0){
			$cek = $this->modelAuth->getData($nama, $password)->result_array();
			foreach ($cek as $login) {
                if($login['status']==2){ 
					$data_log = $this->modelAuth->getLog($login['id_siswa'], $date)->num_rows();
					if($data_log>0){
						$sudah_login = $this->modelAuth->getLog($login['id_siswa'], $date)->result_array();
						foreach($sudah_login as $logout){
							if($logout['logout_tgl']==null){								
								if($logout['ip_address']==$ip){
									$data = array(
										'logout_tgl'=>$date,
										'logout_jam'=>$time,
										'status_login'=>0
									);

									// var_dump($data);
									// echo $logout['id_siswa'];

									$updateLoginToLogout = $this->modelAuth->updateLog('tb_log', $data, $logout['id_siswa'], $logout['login_tgl']);
									if($updateLoginToLogout>=1){
										$this->session->set_flashdata(
											'logout_sukses',
											'<div class="alert alert-success alert-dismissible">
										      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										      <h4><i class="icon fa fa-check"></i> Logout Berhasil !</h4>
										      Anda sudah boleh pulang sekarang.
										    </div>'
										);
										redirect('Auth');
									}else{
										echo "Gagal logout";
									}
								}else{
									$this->session->set_flashdata(
										'gagal_ip',
										'<div class="alert alert-danger alert-dismissible">
									      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									      <h4><i class="icon fa fa-hand-stop-o"></i> Stop</h4>
									      IP yang anda gunakan tidak sama !
									    </div>'
									);
									redirect('Auth');
								}
							}else{
								$this->session->set_flashdata(
									'sudah_logout',
									'<div class="alert alert-info alert-dismissible">
								      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								      <h4><i class="icon fa fa-hand-stop-o"></i> Stop</h4>
								      Anda sudah logout !
								    </div>'
								);
								redirect('Auth');
							}
						}
					}else{
						$data = array(
							'id_siswa'=>$login['id_siswa'],
							'id_sekolah'=>$login['id_sekolah'],
							'login_tgl'=>$date,
							'login_jam'=>$time,
							'status_login'=>1,
							'ip_address'=>$ip
						);

						$simpan_log = $this->modelCore->insertData('tb_log', $data);
						if($simpan_log>=1){
							$this->session->set_flashdata(
								'login_sukses',
								'<div class="alert alert-success alert-dismissible">
							      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							      <h4><i class="icon fa fa-check"></i> Login Berhasil !</h4>
							      Terimakasik sudah login.
							    </div>'
							);
							redirect('Auth');
						}else{
							echo "Gagal input log";
						}
					}
                }else{
                	$this->session->set_flashdata(
						'gagal_aktifasi',
						'<div class="alert alert-warning alert-dismissible">
					      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					      <h4><i class="icon fa fa-hand-stop-o"></i> Stop</h4>
					      Akun belum diaktifasi oleh admin !
					    </div>'
					);
					redirect('Auth');
                }
            }
		}else{
			$this->session->set_flashdata(
				'gagal_uname',
				'<div class="alert alert-danger alert-dismissible">
			      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			      <h4><i class="icon fa fa-hand-stop-o"></i> Stop</h4>
			      Absenmu gagal, <u>username</u> dan <u>password</u> tidak sesuai
			    </div>'
			);
			redirect('Auth');
		}


	}
	public function register()
	{
		$sekolah = $this->modelCore->getData('tb_sekolah');
		$this->load->view('layouts/register', array(
			'sekolah' => $sekolah
		));
	}
	public function simpanSiswa()
	{
		$data = $this->input->post();
		$password = random_string('alnum', 6);
		$ip = $this->input->ip_address();
		$cek = $this->modelAuth->cekSiswa($ip)->num_rows();
		if($cek>0){
			redirect('Auth/register');
		}else{
			$simpan = $this->modelAuth->insertData('tb_siswa', $data, $password);
			if($simpan >= 1){
				$this->session->set_flashdata(
					'pesan',
					'<div class="alert alert-info alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                <h4><i class="icon fa fa-list"></i> Berhasil daftar</h4>
		                Password : <b>'.$password.'</b>
		                <br>
		                Jangan sampai lupa !
		            </div>'
				);
				redirect('Auth');
			}else{
				echo "Gagal";
			}
		}
	}
}
