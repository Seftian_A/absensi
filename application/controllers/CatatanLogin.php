<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CatatanLogin extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('modelCore');
		$this->load->model('modelCatatanLogin');
	}
	public function index()
	{
		$date = date('Y-m-d');
		$data = $this->modelCatatanLogin->getData('WHERE tb_log.login_tgl="'.$date.'"');
		$this->load->view('content/catatanLogin/index', array(
			'data'=>$data
		));
	}
	public function delete($id)
	{
		$where = array(
			'id_log'=>$id
		);

		$delete = $this->modelCore->deleteData('tb_log', 'id_log = '.$where);
		if($delete >= 1){
			redirect('CatatanLogin');
		}else{
			echo "Gagal";
		}
	}
	public function resetLogout($id)
	{
		$data = array(
			'logout_tgl'=>null,
			'logout_jam'=>null,
			'status_login'=>1
		);

		$reset = $this->modelCore->updateData('tb_log', $data, 'id_log = '.$id);
		if($reset >= 1){
			redirect('CatatanLogin');
		}else{
			echo "Gagal";
		}
	}
}
