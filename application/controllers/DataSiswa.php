<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataSiswa extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('modelSiswa');
	}
	public function index()
	{
		$data = $this->modelSiswa->getData();
		$absen = $this->modelSiswa->belumAbsen(date('Y-m-d'));
		$this->load->view('content/dataSiswa/index', array(
			'data'=>$data,
			'absen'=>$absen
		));
	}
}
