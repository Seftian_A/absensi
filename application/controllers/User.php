<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('modelCore');
	}
	public function index()
	{
		$data = $this->modelCore->getData('tb_user');
		$this->load->view('content/user/index',array(
			'data'=>$data
		));
	}
	public function simpan()
	{
		$nama = $_POST['nama'];
		if($_POST['status']==1){
			$status = $_POST['status'];
		}else{
			$status = 2;
		}
		$password = random_string('alnum', 6);

		$data = array(
			'nama_user'=>$nama,
			'password'=>md5($password),
			'password2'=>$password,
			'status'=>$status
		);

		$simpan = $this->modelCore->insertData('tb_user', $data);
		if($simpan >= 1){
			redirect('User');
		}else{
			echo "Gagal";
		}
	}
	public function delete($id)
	{
		$where = array(
			'id_user'=>$id
		);
		$delete = $this->modelCore->deleteData('tb_user', $where);
		if($delete >= 1){
			redirect('User');
		}else{
			echo "Gagal";
		}
	}
}