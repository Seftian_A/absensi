<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url() ?>dist/css/style.css">
<?php endsection('') ?>

<?php section('js') ?>
<?php endsection('') ?>

<?php section('script') ?>
<?php endsection('') ?>

<?php section('content') ?>
<!-- row -->
<div class="row">
    <div class="col-xs-12">
        <div class="box box-sialan">
            <div class="box-header" style="padding-bottom: 30px;">
                <h3 class="box-title"><i class="fa fa-university"></i> Sekolah Terdaftar</h3>
                <ol class="breadcrumb pull-right" style="margin: 0px; padding: 0px; background-color: transparent;">
                    <li><a href="<?= base_url() ?>Dashboard"><i class="fa fa-tv"></i> Dashboard</a></li>
                    <li class="active">Sekolah terdaftar</li>
                </ol>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-hover table-condensed table-sekolah">
                    <thead>
                        <tr>
                            <th width="100">ID</th>
                            <th>Nama Sekolah</th>
                            <th class="text-center" width="110">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <form action="<?= base_url() ?>SekolahTerdaftar/simpan" method="POST">
                            <tr>
                                <td>
                                    <input type="text" class="form-control input-sm" placeholder="Otomatis" disabled>
                                </td>
                                <td>
                                    <input type="text" name="nama_sekolah" class="form-control input-sm" style="width: 100%;" placeholder="Masukkan nama sekolah" required>
                                </td>
                                <td class="text-center">
                                    <button type="submit" class="btn btn-sm bg-olive">MASUKKAN DATA</button>
                                </td>
                            </tr>
                        </form>
                        <?php 
                            $no = 1;
                            foreach($data as $sekolah){ 
                        ?>
                        <tr>
                            <td class="text-center"><?= $no++ ?></td>
                            <td><?= $sekolah['nama_sekolah'] ?></td>
                            <td class="text-center">
                                <a href="<?= base_url() ?>SekolahTerdaftar/delete/<?= $sekolah['id_sekolah'] ?>" class="btn btn-xs bg-maroon hapus-sekolah hapusSekolah">DELETE</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- menampilkan data minggu ini  -->

    <!-- /.col -->
</div>
<?php endsection('') ?>

<?php getview('layouts/theme') ?> 