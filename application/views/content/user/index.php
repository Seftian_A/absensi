<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url() ?>dist/css/style.css">
<?php endsection('') ?>

<?php section('js') ?>
<?php endsection('') ?>

<?php section('script') ?>
<?php endsection('') ?>

<?php section('content') ?>
<!-- row -->
<div class="row">
	<div class="col-xs-12">
		<div class="box box-sialan">
			<div class="box-header" style="padding-bottom: 30px;">
				<h3 class="box-title"><i class="fa fa-user-secret"></i> User <small style="color: #22AC58;">administrator</small></h3>
				<ol class="breadcrumb pull-right" style="margin: 0px; padding: 0px; background-color: transparent;">
			        <li><a href="<?= base_url() ?>Dashboard"><i class="fa fa-tv"></i> Dashboard</a></li>
			        <li class="active">User</li>
			    </ol>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-4">
						<h3 style="margin-top: 50px;"><i class="fa fa-caret-right" style="color: #22AC58;"></i>&nbsp; Data User</h3>
						<code>maksimal password 6 karakter</code>
					</div>
					<div class="col-md-8">
						<table class="table table-hover table-condensed table-style">
							<thead>
								<tr class="bg-olive">
									<th width="100" class="text-center">ID</th>
									<th>Nama</th>
									<th class="text-center">Password</th>
									<th class="text-center">Status</th>
									<th class="text-center" width="110">Aksi</th>
								</tr>
								<form method="POST" action="<?= base_url() ?>User/simpan">
									<tr class="active">
										<td>
											<input type="text" class="form-control input-sm" placeholder="Otomatis" disabled>
										</td>
										<td>
											<input type="text" class="form-control input-sm" name="nama" name style="width: 100%;" placeholder="Masukkan nama user" required>
										</td>
										<td>
											<input type="text" class="form-control input-sm" placeholder="Otomatis" disabled>
										</td>
										<td>
											<select name="status" id="" class="form-control input-sm" name="status">
												<option value="0">--- Status User ---</option>
												<option value="1">Administrator</option>
												<option value="2">Mentor</option>
											</select>
										</td>
										<td class="text-center">
											<button type="submit" class="btn btn-sm bg-olive">MASUKKAN DATA</button>
										</td>
									</tr>							
								</form>
							</thead>
							<tbody>
								<?php 
									$no = 1;
									foreach($data as $user){ 
										if($user['status']==0){
								?>
								<tr class="pointer" style="cursor: pointer;">
									<td class="text-center"><?= $no++ ?></td>
									<td><?= $user['nama_user'] ?></td>
									<td class="text-center"><?= $user['password2'] ?></td>
									<td class="text-center">
										<div class="onoffswitch">
											<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" value="1" id="myonoffswitch" checked>
											<label class="onoffswitch-label" for="myonoffswitch">
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
											</label>
										</div>							
									</td>
									<td class="text-center"></td>
								</tr>
								<?php }else{ ?>
								<tr class="pointer" style="cursor: pointer;">
									<td class="text-center"><?= $no++ ?></td>
									<td><?= $user['nama_user'] ?></td>
									<td class="text-center"><?= $user['password2'] ?></td>
									<td class="text-center">
										<div class="onoffswitch">
											<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" value="1" id="myonoffswitch" <?php if($user['status']==1){echo"checked";} ?>>
											<label class="onoffswitch-label" for="myonoffswitch">
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
											</label>
										</div>							
									</td>
									<td class="text-center">
										<a href="<?= base_url() ?>User/delete/<?= $user['id_user'] ?>" class="btn btn-xs bg-maroon" id="delete">DELETE</a>
									</td>
								</tr>
								<?php }} ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->
</div>
<?php endsection('') ?>

<?php getview('layouts/theme') ?>