<?php section('css') ?>
<?php endsection('') ?>

<?php section('js') ?>
<?php endsection('') ?>

<?php section('content') ?>
<div class="row">
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-aqua">
			<div class="inner">
				<h3><?php foreach($dataSiswa as $s){ echo $s['tot_siswa']; } ?></h3>

				<p>Data Siswa</p>
			</div>
			<div class="icon">
				<i class="ion ion-person"></i>
			</div>
			<a href="<?= base_url() ?>DataSiswa" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-green">
			<div class="inner">
				<h3><?php foreach($dataLog as $l){ echo $l['tot_log']; } ?></h3>

				<p>Login Hari Ini</p>
			</div>
			<div class="icon">
				<i class="ion ion-log-in"></i>
			</div>
			<a href="<?= base_url() ?>CatatanLogin" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-yellow">
			<div class="inner">
				<h3><?php foreach($dataSekolah as $sK){ echo $sK['tot_sekolah']; } ?></h3>

				<p>Sekolah Terdaftar</p>
			</div>
			<div class="icon">
				<i class="ion ion-home"></i>
			</div>
			<a href="<?= base_url() ?>SekolahTerdaftar" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-red">
			<div class="inner">
				<h3><?php foreach($dataUser as $u){ echo $u['tot_user']; } ?></h3>

				<p>User Admin</p>
			</div>
			<div class="icon">
				<i class="ion ion-key"></i>
			</div>
			<a href="<?= base_url() ?>User" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
</div>
<!-- /.row -->
<?php endsection('') ?>

<?php getview('layouts/theme') ?>