<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Absensi Aplication</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?= base_url() ?>dist/plugins/bootstrap-4.3.1/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url() ?>dist/plugins/font-awesome/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url() ?>dist/css/AdminLTEa.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body style="background-color:  #242830;">
    </div>
    <div class="d-flex justify-content-center mt-5 pt-5">
        <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash') ?>"></div>
        <div class="col-lg-11 col-sm-12">
            <div class="row d-flex justify-content-center mt-3">
                <div class="border border-info rounded p-5">
                    <form method="post" action="<?= base_url() ?>LoginAdmin/setLoginAdmin">
                        <span class="text-success d-flex justify-content-center font-weight-lighter text-uppercase text-center ">Login Sebagai Administrator</span>
                        <input name="username" type="text" class="d-block form-control rounded-pill bg-transparent border-success mt-5 text-success" placeholder="Username">
                        <input name="password" type="password" class="d-block form-control rounded-pill bg-transparent border-success mt-5 text-success" placeholder="Password">
                        <button type="submit" class="btn bg-success rounded-pill text-white font-weight-bold shadow-lg col-12 mt-5">login</button>
                    </form>
                    <!-- <a href="<?= base_url() ?>auth/register" class="text-white font-weight-light float-right mt-5">Belum punya akun? Silahkan registrasi</a> -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.login-box -->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>dist/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="<?= base_url() ?>/dist/js/myAlert.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>dist/plugins/bootstrap-4.3.1/dist/js/bootstrap.min.js"></script>
</body>

</html> 