<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url() ?>dist/plugins/datatables/dataTables.bootstrap.css">
<?php endsection('') ?>

<?php section('js') ?>
<script src="<?= base_url() ?>dist/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>dist/plugins/datatables/dataTables.bootstrap.js"></script>
<?php endsection('') ?>

<?php section('script') ?>
<script>
    $(function() {
        //Datatables
        $("#example").dataTable();
    });
</script>
<?php endsection('') ?>
<?php section('content') ?>
<!-- row -->
<?php  
    $bulan = array(
        '01' => 'JANUARI',
        '02' => 'FEBRUARI',
        '03' => 'MARET',
        '04' => 'APRIL',
        '05' => 'MEI',
        '06' => 'JUNI',
        '07' => 'JULI',
        '08' => 'AGUSTUS',
        '09' => 'SEPTEMBER',
        '10' => 'OKTOBER',
        '11' => 'NOVEMBER',
        '12' => 'DESEMBER'
    );
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-sialan">
            <div class="box-header" style="padding-bottom: 30px;">
                <h3 class="box-title"><i class="fa fa-money"></i> Laporan Pembayaran <small>Bulan <?= (strtolower($bulan[date('m')])); ?> <?= date('Y') ?></small></h3>
                <ol class="breadcrumb pull-right" style="margin: 0px; padding: 0px; background-color: transparent;">
                    <li><a href="<?= base_url() ?>Dashboard"><i class="fa fa-tv"></i> Dashboard</a></li>
                    <li class="active">Catatan login</li>

                </ol>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example" class="table table-hover">
                    <thead>
                        <tr>
                            <th class="text-center" width="60">No</th>
                            <th class="text-center">Nama</th>
                            <th class="text-center">Sekolah Asal</th>
                            <th class="text-center">Jumlah Bayar</th>
                            <th class="text-center">Status Lunas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $no = 1;
                            foreach($data as $pembayaran){
                        ?>
                        <tr>
                            <td class="text-center"><?= $no++ ?></td>
                            <td><?= $pembayaran['nama'] ?></td>
                            <td class="text-center"><?= $pembayaran['nama_sekolah'] ?></td>
                            <td>Rp. <?= $pembayaran['tot_bayar'] ?></td>
                            <td class="text-center">
                                <?php if($pembayaran['status_detail']==1){ ?>
                                <button class="btn btn-xs bg-orange">CICIL</button>&nbsp;&nbsp;
                                <?php }elseif($pembayaran['status_detail']==2){ ?>
                                <button class="btn btn-xs bg-maroon">BULAN INI</button>&nbsp;&nbsp;
                                <?php }elseif($pembayaran['status_detail']==3){ ?>
                                <button class="btn btn-xs bg-olive"><i class="fa fa-check"></i> SEMUA BULAN</button>&nbsp;&nbsp;
                                <?php } ?>
                                <button class="btn btn-xs bg-info" data-toggle="modal" data-target="#myModal<?= $pembayaran['id_siswa'] ?>">DETAIL</button>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>

<?php 
    foreach($data as $pembayaran){
?>
<div id="myModal<?= $pembayaran['id_siswa'] ?>" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">
                    <i class="fa fa-list-o"></i>&nbsp; Detail pembayaran
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <label class="control-label col-md-4">Nama</label>
                            <span class="col-md-8">:&nbsp;&nbsp; <?= $pembayaran['nama'] ?></span>
                        </div>
                        <div class="row">
                            <label class="control-label col-md-4">Sekolah</label>
                            <span class="col-md-8">:&nbsp;&nbsp; <?= $pembayaran['nama_sekolah'] ?></span>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 30px;">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr class="success">
                                    <th class="text-center">No</th>
                                    <th class="text-center">Jumlah Bayar</th>
                                    <th class="text-center">Tanggal Bayar</th>
                                    <th class="text-center">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $data = $this->db->where('id_siswa', $pembayaran['id_siswa'])->get('tb_detail_pembayaran')->result_array();
                                    $date = date('m');
                                    $no = 1;
                                    foreach($data as $detail){
                                ?>
                                <tr>
                                    <td class="text-center"><?= $no++ ?></td>
                                    <td class="text-center">Rp. <?= $detail['bayar'] ?></td>
                                    <td class="text-center"><?= $detail['tanggal_bayar'] ?></td>
                                    <td class="text-center">
                                        <?php if($detail['status']==1){ ?>
                                            <button class="btn btn-xs bg-orange">CICIL</button>&nbsp;&nbsp;
                                        <?php }elseif(substr($detail['tanggal_bayar'],5,-3)==$date){ ?>
                                            <button class="btn btn-xs bg-maroon">JULI</button>&nbsp;&nbsp;
                                        <?php }elseif($detail['status']==2){ ?>
                                            <button class="btn btn-xs bg-olive"><i class="fa fa-check"></i> SEMUA BULAN</button>&nbsp;&nbsp;
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<?php } ?>
<?php endsection('') ?>

<?php getview('layouts/theme') ?> 