<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url() ?>dist/plugins/datatables/dataTables.bootstrap.css">
<!-- daterange picker -->
<link rel="stylesheet" href="<?= base_url() ?>dist/plugins/daterangepicker/daterangepicker-bs3.css">
<link rel="stylesheet" href="<?= base_url() ?>dist/css/style.css">
<?php endsection('') ?>

<?php section('js') ?>
<script src="<?= base_url() ?>dist/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>dist/plugins/datatables/dataTables.bootstrap.js"></script>
<?php endsection('') ?>

<?php section('script') ?>
<script>
    $(function() {
        $('#example').dataTable();
    })
</script>
<?php endsection('') ?>

<?php section('content') ?>
<!-- row -->
<div class="row">
    <div class="col-xs-12">
        <div class="box box-sialan">
            <div class="box-header" style="padding-bottom: 30px;">
                <h3 class="box-title"><i class="fa fa-users"></i> Data Siswa</h3>
                <ol class="breadcrumb pull-right" style="margin: 0px; padding: 0px; background-color: transparent;">
                    <li><a href="<?= base_url() ?>Dashboard"><i class="fa fa-tv"></i> Dashboard</a></li>
                    <li class="active">Data Siswa</li>
                </ol>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example" class="table table-hover table-condensed table-sekolah">
                    <thead>
                        <tr>
                            <th>Data Siswa</th>
                        </tr>
                    </thead>
                        <?php foreach($data as $siswa){ ?>
                        <tr>
                            <td style="padding: 1.5rem;">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="nama">Nama</label> : <?= $siswa['nama'] ?><br>
                                        <label for="password">Password</label> : <?= $siswa['password2'] ?><br>
                                        <label for="ip_address">IP Address</label> : <?= $siswa['ip_address'] ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="jk">Jenis Kelamin</label> : <?php if($siswa['jk']==1){echo"Laki-Laki";}else{echo"Perempuan";} ?><br>
                                        <label for="sekolah">Sekolah</label> : <?= $siswa['nama_sekolah'] ?><br>
                                        <label for="ttl">Tempat Tanggal Lahir</label> : <?= $siswa['tempat_lahir'] ?>, <?= $siswa['tanggal_lahir'] ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12" style="margin:1rem 0;">
                                        <button class="btn btn-xs bg-maroon hapus-siswa" >DELETE</button>
                                        &nbsp;<button class="btn btn-xs bg-olive">CETAK ULANG PASSWORD</button>
                                        <?php if($siswa['status']==1){ ?>
                                        &nbsp;<button class="btn btn-xs bg-orange"><i class="fa fa-check"></i> KONFIRMASI</button>
                                        <?php } ?>
                                        <!-- &nbsp;<button class="btn btn-xs bg-danger"><b>!</b>&nbsp; BELUM ABSEN</button> -->
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<?php endsection('') ?>

<?php getview('layouts/theme') ?>