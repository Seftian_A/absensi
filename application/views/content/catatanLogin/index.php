<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url() ?>dist/plugins/datatables/dataTables.bootstrap.css">
<!-- daterange picker -->
<link rel="stylesheet" href="<?= base_url() ?>dist/plugins/daterangepicker/daterangepicker-bs3.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?= base_url() ?>dist/plugins/datepicker/datepicker3.css">
<?php endsection('') ?>

<?php section('js') ?>
<script src="<?= base_url() ?>dist/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>dist/plugins/datatables/dataTables.bootstrap.js"></script>
<!-- date-range-picker -->
<script src="<?= base_url() ?>dist/plugins/momentjs/moment.min.js"></script>
<script src="<?= base_url() ?>dist/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url() ?>dist/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.2/sweetalert2.all.js"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
<?php endsection('') ?>

<?php section('script') ?>
<script>
    $(function() {
        //Datatables
        $("#example").dataTable();
        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'MM/DD/YYYY h:mm A'
        });
        //Date range as a button
        $('#daterange-btn').daterangepicker({
                ranges: {
                    'Hari ini': [moment(), moment()],
                    'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 hari lalu': [moment().subtract(6, 'days'), moment()],
                    '30 hari lalu': [moment().subtract(29, 'days'), moment()],
                    'Bulan ini': [moment().startOf('month'), moment().endOf('month')],
                    'Bulan lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate: moment()
            },
            function(start, end) {
                $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
        );
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
        })
        //Date picker
        $('#datepicker1').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
        });
    });
</script>
<?php endsection('') ?>
<?php section('content') ?>
<!-- row -->
<?php 
    function hari_ini(){
        $hari = date ("D");

        switch($hari){
            case 'Sun':
                $hari_ini = "Minggu";
            break;

            case 'Mon':         
                $hari_ini = "Senin";
            break;

            case 'Tue':
                $hari_ini = "Selasa";
            break;

            case 'Wed':
                $hari_ini = "Rabu";
            break;

            case 'Thu':
                $hari_ini = "Kamis";
            break;

            case 'Fri':
                $hari_ini = "Jumat";
            break;

            case 'Sat':
                $hari_ini = "Sabtu";
            break;
            
            default:
                $hari_ini = "Tidak di ketahui";     
            break;
        }

        return $hari_ini;

    }
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-sialan">
            <div class="box-header" style="border-bottom: none;">
                <h3 class="box-title"><i class="fa fa-list-ul"></i> Catatan Login <small><?= hari_ini() ?></small></h3>
                <ol class="breadcrumb pull-right" style="margin: 0px; padding: 0px; background-color: transparent;">
                    <li><a href="<?= base_url() ?>Dashboard"><i class="fa fa-tv"></i> Dashboard</a></li>
                    <li class="active">Catatan login</li>

                </ol>
            </div>
            <div class="box-header" style="padding-bottom: 0px; padding-left: 0px;">
                <form action="#" method="POST">
                    <div class="form-group col-md-1" style="margin-left: -5px;">
                        <div class="input-group">
                            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-file-excel-o"></i> Ekspor
                            </button>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <div class="input-group">
                            <div class="btn-group">
                                <button type="button" class="btn btn-sm btn-info pull-left" id="daterange-btn">
                                    <span>
                                        <i class="fa fa-calendar"></i>&nbsp; Filter berdasar &nbsp;
                                    </span>
                                    <i class="fa fa-caret-down"></i>
                                </button>
                                <button type="submit" class="btn btn-sm btn-info"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example" class="table table-hover table-condensed table-sekolah">
                    <thead>
                        <tr>
                            <th class="text-center" width="60">No</th>
                            <th class="text-center">Nama</th>
                            <th class="text-center">Sekolah Asal</th>
                            <th class="text-center">IP Address</th>
                            <th class="text-center">Login</th>
                            <th class="text-center">Logout</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $no = 1;
                            foreach($data as $log){
                        ?>
                        <tr>
                            <td class="text-center"><?= $no++ ?></td>
                            <td><?= $log['nama'] ?></td>
                            <td class="text-center"><?= $log['nama_sekolah'] ?></td>
                            <td class="text-center"><?= $log['ip_address'] ?></td>
                            <td class="text-center"><?= $log['login_tgl'] ?>, <?= $log['login_jam'] ?></td>
                            <td class="text-center"><?= $log['logout_tgl'] ?>, <?= $log['logout_jam'] ?></td>
                            <td class="text-center">
                                <a href="<?= base_url() ?>CatatanLogin/delete/<?= $log['id_log'] ?>" class="btn btn-xs bg-maroon hapus-sekolah hapusCttLog" data-id="" name="" class="btn btn-xs btn-danger bg-maroon hapusCttLog">DELETE</a> &nbsp;&nbsp;
                                <a href="<?= base_url() ?>CatatanLogin/resetLogout/<?= $log['id_log'] ?>" class="btn btn-xs btn-default hapus-sekolah hapusCttLog" data-id="" name="" class="btn btn-xs btn-danger bg-maroon hapusCttLog"><i class="fa fa-refresh"></i> RESET LOGOUT</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">
                    <i class="fa fa-file-excel-o"></i>&nbsp; Ekspor data
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-7">
                        <a href="<?= base_url() ?>CatatanLogin/cetak" class="btn btn-sm bg-olive">Ekspor semua data</a>
                    </div>
                    <div class="form-group col-md-5">
                        <label>Semua data</label><br>
                        <code style="font-size: 11px;">
                            * Data akan di ekspor dalam 1 file dan sudah di sortir per-hari.
                        </code>
                    </div>
                </div>
                <hr style="height: 1px; width: 100%; background-color: #22AC58;">
                <div class="row">
                    <div class="form-group col-md-7">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <a href="<?= base_url() ?>CatatanLogin/getCetak/<?= date("Y-m-d") ?>" class="btn btn-sm bg-olive">Ekspor data hari ini</a>
                            </div>
                            <div class="form-group col-md-12">
                                <a href="<?= base_url() ?>CatatanLogin/getCetak/<?= date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - 1, date('Y'))) ?>" class="btn btn-sm bg-olive">Ekspor data kemarin</a>
                            </div>
                            <div class="form-group col-md-12">
                                <a href="<?= base_url() ?>CatatanLogin/getCetak/<?= $day ?>/<?= date("Y-m-d") ?>" class=" btn btn-sm bg-olive">Ekspor data minggu ini</a>
                            </div>
                            <div class="form-group col-md-12">
                                <a href="<?= base_url() ?>CatatanLogin/getCetak/<?= date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y'))) ?>/<?= date('Y-m-d', mktime(0, 0, 0, date('m') + 1, 1 - 1, date('Y'))) ?>" class="btn btn-sm bg-olive">Ekspor data bulan ini</a>
                            </div>
                            <form class="form-group col-md-12" method="POST" action="<?= base_url('CatatanLogin/cetakDenganTanggal') ?>">
                                <div class="input-group input-group-sm">
                                    <input type="text" class="form-control pull-right" id="datepicker" name="date1">
                                    <div class="input-group-addon">
                                        to
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker1" name="date2">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn bg-olive btn-flat">Ekspor</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="form-group col-md-5">
                        <label>Opsional ekspor</label><br>
                        <code style="font-size: 11px;">
                            * Data akan di ekspor dalam 1 file dan sudah di sortir per-hari degan jumlah range data sebagaimana yang dipilih atau di-inputkan.
                        </code>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<?php endsection('') ?>

<?php getview('layouts/theme') ?> 