<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url() ?>dist/plugins/datatables/dataTables.bootstrap.css">
<?php endsection('') ?>

<?php section('js') ?>
<script src="<?= base_url() ?>dist/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>dist/plugins/datatables/dataTables.bootstrap.js"></script>
<?php endsection('') ?>

<?php section('script') ?>
<script>
    $(function() {
        //Datatables
        $("#example").dataTable();

        $(document).on('click', '#cicil', function(){
            var target = $(this).attr('data-target');
            var bayar = $('#rupiah'+target).val();
            var id_siswa = $('#id_siswa'+target).val();
            var data = {id_siswa:id_siswa,jumlah_bayar:bayar};
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url("Pembayaran/cicil"); ?>',
                data: data,
                success: function(){
                    alert('Data berhasil masuk');
                }
            });
        });
        $(document).on('click', '#bulan_ini', function(){
            var target = $(this).attr('data-target');
            var bayar = $('#rupiah'+target).val();
            var id_siswa = $('#id_siswa'+target).val();
            var data = {id_siswa:id_siswa,jumlah_bayar:bayar};
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url("Pembayaran/bulanIni"); ?>',
                data: data,
                success: function(){
                    alert('Data berhasil masuk');
                }
            });
        });
        $(document).on('click', '#semua_bulan', function(){
            var target = $(this).attr('data-target');
            var bayar = $('#rupiah'+target).val();
            var id_siswa = $('#id_siswa'+target).val();
            var data = {id_siswa:id_siswa,jumlah_bayar:bayar};
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url("Pembayaran/semuaBulan"); ?>',
                data: data,
                success: function(){
                    alert('Data berhasil masuk');
                    location.reload();
                }
            });
        });
    });
</script>
<?php endsection('') ?>
<?php section('content') ?>
<!-- row -->
<?php  
    $bulan = array(
        '01' => 'JANUARI',
        '02' => 'FEBRUARI',
        '03' => 'MARET',
        '04' => 'APRIL',
        '05' => 'MEI',
        '06' => 'JUNI',
        '07' => 'JULI',
        '08' => 'AGUSTUS',
        '09' => 'SEPTEMBER',
        '10' => 'OKTOBER',
        '11' => 'NOVEMBER',
        '12' => 'DESEMBER'
    );
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-sialan">
            <div class="box-header" style="padding-bottom: 30px;">
                <h3 class="box-title"><i class="fa fa-money"></i> Pembayaran <small>Bulan <?= (strtolower($bulan[date('m')])); ?> <?= date('Y') ?></small></h3>
                <ol class="breadcrumb pull-right" style="margin: 0px; padding: 0px; background-color: transparent;">
                    <li><a href="<?= base_url() ?>Dashboard"><i class="fa fa-tv"></i> Dashboard</a></li>
                    <li class="active">Catatan login</li>

                </ol>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example" class="table table-hover">
                    <thead>
                        <tr>
                            <th class="text-center" width="60">No</th>
                            <th class="text-center">Nama</th>
                            <th class="text-center">Sekolah Asal</th>
                            <th class="text-center">Jumlah Bayar</th>
                            <th class="text-center">Status Lunas</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php 
                            $no = 1;
                            foreach($data as $pembayaran){
                        ?>
                        <tr>
                            <td class="text-center"><?= $no++ ?></td>
                            <td><?= $pembayaran['nama'] ?></td>
                            <td class="text-center"><?= $pembayaran['nama_sekolah'] ?></td>
                            <td>
                                Rp. <input type="text" class="form-control" id="rupiah<?= $pembayaran['id_siswa'] ?>" name="jumlah_bayar" style="text-align: right;" placeholder="Jumlah bayar">
                                <input type="hidden" id="id_siswa<?= $pembayaran['id_siswa'] ?>" name="id_siswa" value="<?= $pembayaran['id_siswa'] ?>">
                            </td>
                            <td class="text-center">                                
                                <?php if($pembayaran['status_bayar']!=2){ ?>
                                    <button class="btn btn-xs bg-orange" id="cicil" data-target="<?= $pembayaran['id_siswa'] ?>">CICIL</button>&nbsp;&nbsp;
                                    <?php  
                                        $date = date('m');
                                        if(substr($pembayaran['tanggal_bayar'],5,-3)!=$date){
                                    ?>
                                        <button class="btn btn-xs bg-maroon" id="bulan_ini" data-target="<?= $pembayaran['id_siswa'] ?>">BULAN INI</button>&nbsp;&nbsp;
                                    <?php } ?>
                                    <button class="btn btn-xs bg-olive" id="semua_bulan" data-target="<?= $pembayaran['id_siswa'] ?>"><i class="fa fa-check"></i> SEMUA BULAN</button>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<?php endsection('') ?>

<?php getview('layouts/theme') ?> 