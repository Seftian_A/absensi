<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Absensi Aplication</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= base_url() ?>dist/plugins/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url() ?>dist/plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ion Icons -->
  <link rel="stylesheet" href="<?= base_url() ?>dist/plugins/ionicons-2.0.1/css/ionicons.min.css">
  <!-- render css -->
  <?php render('css') ?>
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url() ?>dist/css/AdminLTEa.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
  <link rel="stylesheet" href="<?= base_url() ?>dist/css/skins/_all-skins.css">
  <!-- Pace style -->
  <link rel="stylesheet" href="<?= base_url() ?>dist/plugins/pace/pace.min.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
  <!-- Main Header -->
  <header class="main-header">
    <!-- Logo -->
    <a href="<?= base_url() ?>dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><i class="fa fa-list-alt"></i></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">Absensi<small style="font-size: 10px; color: #70b000;">_administrator</small></span>
    </a>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= base_url() ?>dist/img/usr.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>usr Badruz Zaman</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">MENU</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="<?php if($this->uri->segment(1) == 'Dashboard'){echo"active";} ?>">
          <a href="<?= base_url() ?>Dashboard">
            <i class="fa fa-tv"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(1) == 'CatatanLogin'){echo"active";} ?>">
          <a href="<?= base_url() ?>CatatanLogin">
            <i class="fa fa-list-ul"></i>
            <span>Catatan Login</span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(1) == 'SekolahTerdaftar'){echo"active";} ?>">
          <a href="<?= base_url() ?>SekolahTerdaftar">
            <i class="fa fa-university"></i>
            <span>Sekolah Terdaftar</span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(1) == 'DataSiswa'){echo"active";} ?>">
          <a href="<?= base_url() ?>DataSiswa">
            <i class="fa fa-users"></i>
            <span>Data Siswa</span>
            <span class="countData">
            </span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(1) == 'User'){echo"active";} ?>">
          <a href="<?= base_url() ?>User">
            <i class="fa fa-users"></i>
            <span>User</span>
            <span class="countData">
            </span>
          </a>
        </li>
        <li class="treeview <?php if($this->uri->segment(1) == 'Pembayaran' || $this->uri->segment(1) == 'LaporanPembayaran'){echo"active";} ?>">
          <a href="#">
              <i class="fa fa-money"></i>
              <span>Keuangan</span>
              <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
              <li class="<?php if($this->uri->segment(1) == 'Pembayaran'){echo"active";} ?>"><a href="<?= base_url() ?>Pembayaran"><i class="fa fa-caret-right"></i> Pembayaran</a></li>
              <li class="<?php if($this->uri->segment(1) == 'LaporanPembayaran'){echo"active";} ?>"><a href="<?= base_url() ?>LaporanPembayaran"><i class="fa fa-caret-right"></i> Laporan Pembayaran</a></li>
          </ul>
        </li>
        <li>
          <a href="<?= base_url() ?>dashboard">
            <i class="fa fa-sign-out"></i>
            <span>Keluar</span>
          </a>
        </li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <?php render('content') ?>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>
<!-- ./wrapper -->
<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<script src="<?= base_url() ?>dist/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= base_url() ?>dist/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?= base_url() ?>dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<?php render('js') ?>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>dist/js/app.min.js"></script>
<?php render('script') ?>
</body>
</html>
