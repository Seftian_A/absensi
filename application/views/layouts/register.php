<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Absensi D-Junsoft</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?= base_url() ?>dist/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url() ?>dist/plugins/font-awesome/css/font-awesome.min.css">
    <!-- DatePicker -->
    <link rel="stylesheet" href="<?= base_url() ?>dist/plugins/datepicker/datepicker3.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url() ?>dist/css/AdminLTEa.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="hold-transition register-page">
    <div class="register-box">
        <!-- /.register-logo -->
        <div class="register-box-body">
            <p class="login-box-msg">Silahkan mengisi form berikut</p>
            <form method="post" action="<?= base_url() ?>Auth/simpanSiswa">
                <div class="form-group has-feedback">
                    <input type="text" name="nama" class="form-control" placeholder="Nama" required>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <div class="form-group">
                        <select class="form-control" name="jk" required>
                            <option value="">-- Jenis Kelamin --</option>
                            <option value="1">Laki - Laki</option>
                            <option value="2">Perempuan</option>
                        </select>
                    </div>
                </div>
                <div class="form-group has-feedback">
                    <div class="form-group">
                        <select class="form-control" name="id_sekolah" required>
                            <option value="">-- Pilih Sekolah --</option>
                            <?php foreach($sekolah as $sK){ ?>
                                <option value="<?= $sK['id_sekolah'] ?>"><?= $sK['nama_sekolah'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" required>
                    <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" name="tanggal_lahir" id="datepicker" placeholder="Tanggal Lahir" class="form-control" required>
                    <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-success btn-block btn-flat">Daftarkan Diri</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <br />
            <a href="<?= base_url() ?>auth" class="text-center">Kembali ke laman login</a>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->

    <!-- jQuery 2.2.3 -->
    <script src="<?= base_url() ?>dist/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?= base_url() ?>dist/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- DatePicker -->
    <script src="<?= base_url() ?>dist/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script>
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
        });                      
    </script>
</body>
</html>
