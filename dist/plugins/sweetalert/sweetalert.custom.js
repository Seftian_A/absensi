function swalConfirm(text, action) {
	swal({
	  title: 'Apakah anda yakin?',
	  text: text,
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#dd4b39",
	  confirmButtonText: "OK"	  
	},action);
}